import axios from 'axios'

export default {
  /*
  ** Generate Dynamic Routes
  */
 generate: {
  routes: function () {
    return axios.get('https://api.myjson.com/bins/acq2n')
    .then((res) => {
      return res.data.map((post) => {
        return '/posts/' + post.id + '/' + post.permalink
      })
    })
  }
 },
 
  mode: 'universal',
  /*
  ** Headers of the page
  */
 head: {
  title: 'Ultra Brands',
  meta: [
    { charset: 'utf-8' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    { name: 'keywords', content: 'web design, website design, web developer, cheshire, vue.js, nuxt.js, wordpress, php, javascript'},
    { hid: 'description', name: 'description', content: 'Web Designer & Developer Cheshire'},
    {hid: 'og:title', name: 'og:title', content: 'This is Ultra'},
    {hid: 'og:description', name: 'og:description', content: 'Web Designer & Website Developer Cheshire | ultr4.co.uk'}
  ],
},
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
 css: [
  '~assets/css/main.css'
],
  /*
  ** Plugins to load before mounting the App
  */
 plugins: [
  {src: '@/assets/js/main.js', ssr: false}
],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
